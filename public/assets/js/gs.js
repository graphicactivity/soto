/**
 * Created by luizhv on 16/05/17.
 */
$(document).ready(function() {
    var cities = [];
    var suburbs = [];

    var stateFromList = false;
    var suburbFromList = false;

    $.get('/assets/js/city.json', [], function(data) {
        cities = data;
        $('#State').autocomplete({
            lookup: cities,
            onSearchStart: function(query) {
                stateFromList = false;
            },
            onSelect: function(suggestion) {
                stateFromList = true;
                console.log('You selected: ' + suggestion.value);
            }
        });
    }, 'json');

    $.get('/assets/js/suburb.json', [], function(data) {
        suburbs = data;
        $('#Suburb').autocomplete({
            lookup: suburbs,
            onSearchStart: function(query) {
                suburbFromList = false;
            },
            onSelect: function(suggestion) {
                suburbFromList = true;
                console.log('You selected: ' + suggestion.value);
            }
        });
    }, 'json');

    $('#State').focusout(function(e) {
        if (!stateFromList) {
            $(this).val('');
        }
    });

    $('#Suburb').focusout(function(e) {
        if (!suburbFromList) {
            $(this).val('');
        }
    });
});
