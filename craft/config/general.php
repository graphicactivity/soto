<?php

return array(
    '*' => array(
		'siteUrl' => null,
		'defaultWeekStartDay' => 0,
		'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
		'cpTrigger' => 'admin',
        'allowAutoUpdates' => false,
        'generateTransformsBeforePageLoad' => true,
    ),

    'soto.local' => array(
        'devMode' => true,
        'environmentVariables' => array(
        	'basePath' => '/Users/GraphicActivity/Sites/soto.local/public/',
            'baseUrl'  => 'http://soto.local/',
        )
    ),

    'soto.co.nz' => array(
        'devMode' => false,
        'environmentVariables' => array(
        	'basePath' => '/var/www/html/',
            'baseUrl'  => 'https://soto.co.nz/',
        )
    )
);
