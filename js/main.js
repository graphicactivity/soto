// Init
$(document).ready(function(){
    // Menu button
	$('.menu').click(function(){
		$(this).toggleClass('open');
        $('.reg').toggleClass('open');
        $('.fullMenu').toggleClass('openMenu');
	});
    $('.menu.but.clo').click(function(){
        $('.menu.but.open').removeClass('open');
		//$('#navIcon').removeClass('open');
        $('.fullMenu').removeClass('openMenu');
	});
    // Register slide out
    $('.register').click(function(){
        //$(this).toggleClass('slideOpen');
        $('body').toggleClass('stillPage');
        $('.registerMenu').toggleClass('openRegister');
        $('.fullMenu').removeClass('openMenu');
	});
    // Register close
    $('.regClose').click(function(){
        $('.registerMenu').removeClass('openRegister');
        $('.reg').removeClass('open');
        $('body').removeClass('stillPage');
        $('.menu.but.open').removeClass('open');
        $('.fullMenu').removeClass('openMenu');
    });
    // Page Scroll to ID
    $("a[href*='#']").mPageScroll2id();
    // Lightbox init
    $('.swipebox').swipebox({
        loopAtEnd: true
    });
    // Wow js
    new WOW().init();
    var config = {
      '.chosen-select'           : {max_selected_options: 1},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:'100%'}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
    // Show/Hide Costing options
    $('.valueChangedOne').click(function() {
        $("li.price-range-a").removeClass("remove");
        $("li.price-range-b").addClass("remove");
        $("li.price-range-c").addClass("remove");
        $("li.price-range-d").addClass("remove");
        $(".budget").removeClass("remove");
    });
    $('.valueChangedTwoOne').click(function() {
        $("li.price-range-a").addClass("remove");
        $("li.price-range-b").removeClass("remove");
        $("li.price-range-c").addClass("remove");
        $("li.price-range-d").addClass("remove");
        $(".budget").removeClass("remove");
    });
    $('.valueChangedTwoTwo').click(function() {
        $("li.price-range-a").addClass("remove");
        $("li.price-range-b").addClass("remove");
        $("li.price-range-c").removeClass("remove");
        $("li.price-range-d").addClass("remove");
        $(".budget").removeClass("remove");
    });
    $('.valueChangedTwoThree').click(function() {
        $("li.price-range-a").addClass("remove");
        $("li.price-range-b").addClass("remove");
        $("li.price-range-c").addClass("remove");
        $("li.price-range-d").removeClass("remove");
        $(".budget").removeClass("remove");
    });
    $(function(){
    $('#DesktopForm').submit(function(){
         var options = $('#choose-one > option:selected');
         if(options.length == 0){
             alert('Please choose an option, How did you hear about Soto');
             return false;
         }
         if (grecaptcha.getResponse() == "") {
             return false;
         }
    });
    $('#DesktopForm').submit(function(){
         var options = $('#choose-two > option:selected');
         if(options.length == 0){
             alert('Please choose an option, Purchase intention');
             return false;
         }
         if (grecaptcha.getResponse() == "") {
             return false;
         }
    });
});

validateForms();

function validateForms() {
    var desktopVisible = $('#desktopFormBlock').is(':visible');
    console.log("Desktop Visible = %s", desktopVisible);

    validateForm('DesktopForm');
}

function validateForm(formName) {
    var form_ID = '#' +formName;
    var errordiv_ID = '#errordiv' + formName;
    var submitbutton_ID = '#submitButton' + formName;

    //hidden to start with
    $(errordiv_ID).hide();


    console.log("validateForm %s", form_ID);

    jQuery.validator.addMethod(
        "phone",
        function(phone_number, element) {
            return this.optional(element) || /^[ ().+-]*([0-9][ ().+-]*){8,12}$/.test(phone_number);
        }, "- Please enter a valid phone number, 8-12 digits long."
    );


    $(form_ID).validate({
        focusInvalid: false, //stops the form jumping to highlight the selected field
        onfocusout: false, //validates on each element change
        rules: {
            //contact_name
            "FirstName": {
                required: true
            },
            //contact_lastname
            "LasteName": {
                required: true
            },
            //contact_email
            "Emailaddress": {
                required: true,
                email: true
            },
            //contact_phone
            "PhoneNumber": {
                required: true,
                phone: true
            },
            //contact_suburb
            "Suburb": {
                required: true
            },
            //contact_city
            "State": {
                required: true
            },
            //How did you hear about
            "hearaboutus": {
                required: true
            },
            //Purchase Intention?
            "PropertyUse": {
                required: true
            },
            //Interested In?
            "Noofbedrooms": {
                required: true
            },
            //Budget Indication?
            "Minpreferredpricerange": {
                required: true
            }
        },
        messages: {
            //contact_name
            "FirstName": {
                required: "Please enter your first name. "
            },
            //contact_lastname
            "LasteName": {
                required: "Please enter your last name. "
            },
            //contact_email
            "Emailaddress": {
                required: "Please enter your email address. ",
                email: "Please enter a valid email address. "
            },
            //contact_phone
            "PhoneNumber": {
                required: "Please enter your phone number. "
            },
            //contact_suburb
            "Suburb": {
                required: "Please enter your suburb. "
            },
            //contact_city
            "State": {
                required: "Please enter your city. "
            },
            //How did you hear about Soto?
            "hearaboutus": {
                required: "Please select at least one way you heard about us. "
            },
            //Purchase Intention?
            "PropertyUse": {
                required: "Please select your purchase intention. "
            },
            //Interested In?
            "Noofbedrooms": {
                required: "Please select what property type you are interested in. "
            },
            //Budget Indication?
            "Minpreferredpricerange": {
                required: "Please select your budget indication. "
            }
        },
        highlight: function(element) {
            $(element).removeClass("error");
        },
        errorPlacement: function(error, element) {
            console.log("%s errorPlacement begin ", $('form').attr('id'));

            $(errordiv_ID).show();
            error.appendTo(errordiv_ID);
        },
        submitHandler: function(form) {
            //prevent double form submission.
            $(submitbutton_ID).prop('value', 'Submitting...');
            $(submitbutton_ID).prop('disabled', true);
            $(submitbutton_ID).css("cursor", "default");

            //this runs when the form validated successfully
            //$(form_ID).submit();
            //form.submit(); //submit it the form

            console.log("submitHandler - valid after submit");
            submitted = true;
            return true;
        },
        invalidHandler: function(f, v) {
            var errors = v.numberOfInvalids();
            if (errors) {
                var invalidElements = v.invalidElements();

                console.log("Submit validation failed: {0}", invalidElements[0]);
            } else {
                $(errordiv_ID).hide();
            }

            submitted = false;
            return false;
        }
    });
}
});

// Hide arrow on scroll
$(window).scroll(function () {
    var scrollTop = $(window).scrollTop();
    var height = $(window).height();
    $('.downArrow').css({
        'opacity': ((height - scrollTop) / height)
    });
});
